import java.util.*;

public class Main {
    
    public Map<Character, Set<String>> indexAndMap(String input) {
        Map<Character, Set<String>> output = new TreeMap<>();
        if (!input.isEmpty()) {
            input = input.replaceAll("[^a-zA-Z\\s]", "").toLowerCase();
            Set<String> words = new TreeSet<>(Arrays.asList(input.split(" ")));
            Set<Character> letters = new TreeSet<>();
            for (char c : input.replaceAll(" ", "").toCharArray()) {
                letters.add(c);
            }
            letters.forEach(letter -> {
                Set<String> wordsContainingLetter = new TreeSet<>();
                words.forEach(word -> {
                    if (word.indexOf(letter) != -1) {
                        wordsContainingLetter.add(word);
                    }
                    output.put(letter, wordsContainingLetter);
                });
            });
        }
        return output;
    }
}
