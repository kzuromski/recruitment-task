import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class MainTest
{
    @Test
    public void shouldConfirmMatchTest() {
        Map<Character, Set<String>> testInput = new TreeMap<>() {{
            put('a', new TreeSet<>(Arrays.asList("ala", "javie", "kota", "ma")));
            put('d', new TreeSet<>(Arrays.asList("koduje")));
            put('e', new TreeSet<>(Arrays.asList("javie", "koduje")));
            put('i', new TreeSet<>(Arrays.asList("javie")));
            put('j', new TreeSet<>(Arrays.asList("javie", "koduje")));
            put('k', new TreeSet<>(Arrays.asList("koduje", "kot", "kota")));
            put('l', new TreeSet<>(Arrays.asList("ala")));
            put('m', new TreeSet<>(Arrays.asList("ma")));
            put('o', new TreeSet<>(Arrays.asList("koduje", "kot", "kota")));
            put('t', new TreeSet<>(Arrays.asList("kot", "kota")));
            put('u', new TreeSet<>(Arrays.asList("koduje")));
            put('v', new TreeSet<>(Arrays.asList("javie")));
            put('w', new TreeSet<>(Collections.singletonList("w")));
        }};
        assertTrue(testInput.equals(new Main().indexAndMap("ala ma kota, kot koduje w Javie kota")));
    }

    @Test
    public void shouldConfirmMatchForEmptyStringTest() {
        assertTrue(new TreeMap<Character, Set<String>>().equals(new Main().indexAndMap("")));
    }
}
